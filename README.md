# TERN Linked Data Landing Page

This repository contains the version-controlled HTML source files for TERN's Linked Data landing pages. 

## Future Improvements
Create a simple Python script to read in a YAML configuration file to generate the HTML landing page. The YAML file will look something like this:
```yaml
ontologies: 
    Plot Ontology:
      url: http://linked.data.gov.au/def/plot
    Datatype Ontology:
      url: http://linked.data.gov.au/def/datatype

vocabularies:
    CORVEG Vocabularies:
      url: http://linkeddata.tern.org.au/viewer/corveg
      owner_name: Department of Environment and Science (DES)
      owner_url: https://www.des.qld.gov.au/
    TERN Vocabularies:
      url: http://linkeddata.tern.org.au/viewer/tern
      owner_name: Terrestrial Ecosystem Research Network (TERN)
      owner_url: https://www.tern.org.au/

```

## Author
**Edmond Chuc**  
*Software Engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  
